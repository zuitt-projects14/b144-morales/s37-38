const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},
	age: {
		type: String,
		required: [true, "Age is required"]
	},
	gender: {
		type: String,
		required: [true, "Gender is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNumber: {
		type: String,
		required: [true, "Mobile Number is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	cart: [
    {
      
        productId: {
          type: String,
        },
        quantity: { 
        type: Number
      },
      	userId: {
      	type: String
      },

      price: {
      	type: Number,
      },
      totalAmount: {
      	type: Number
      }


      },
      
     
    ],
	order: [
	{
		productId: {
			type: String,
			
		},
		orderedOn: {
			type: Date,
			default: new Date
		},
		status: {
			type: String,
			default: "Ordered"
		},
		quantity: {
			type: Number
		},
		price: {
			type: Number
		},
		totalPrice: {
			type:Number
		}
	}
	]
})


userSchema.methods.addToCart = function(product) {
  const cartProductIndex = this.cart.items.findIndex(cp => {
    return cp.productId.toString() === product._id.toString();
  });
  let newQuantity = 1;
  const updatedCartItems = [...this.cart.items];

  if (cartProductIndex >= 0) {
    newQuantity = this.cart.items[cartProductIndex].quantity + 1;
    updatedCartItems[cartProductIndex].quantity = newQuantity;
  } else {
    updatedCartItems.push({
      productId: product._id,
      quantity: newQuantity
    });
  }
  const updatedCart = {
    items: updatedCartItems
  };
  this.cart = updatedCart;
  return this.save();
};

userSchema.methods.removeFromCart = function(productId) {
  const updatedCartItems = this.cart.items.filter(item => {
    return item.productId.toString() !== productId.toString();
  });
  this.cart.items = updatedCartItems;
  return this.save();
};

userSchema.methods.clearCart = function() {
  this.cart = { items: [] };
  return this.save();
};

module.exports = mongoose.model('User', userSchema);



