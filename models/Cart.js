const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const cartSchema = new Schema ({

product: { type: String, 
	ref: "product" 
},
name: {
	type: String,

},



quantity: 
{ type: Number, 
	default: 1, 
	required: true 
},
totalAmount: {
	type: Number
},
email:{
	type:"String"
},
price: {
	type: Number
},
addedOn: {
	type: Date
},
userId: {
	type: String,
	 ref: "cart"
}

})


module.exports = mongoose.model("Cart", cartSchema);