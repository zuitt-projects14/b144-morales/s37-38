const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const orderSchema = new Schema ({
	
	userId: {
		type: String
	},
	product: 
		{ type: String, 
	ref: "product" 
		},
	productId: 
		{ type: String, 
	ref: "product" 
		},
	quantity: 
		{ type: Number, 
		default: 1, 
		required: true 
		},
	totalAmount: {
		type: Number
		},
	email:{
		type:"String"
		},
	price: {
		type: Number
		},
	purchasedOn: {
		type: Date
		},

	user: {
      	type: mongoose.Schema.Types.ObjectId,
      	ref: "User",
    		},




})


module.exports = mongoose.model("Order", orderSchema);





//mongoose.Schema.Types.ObjectId