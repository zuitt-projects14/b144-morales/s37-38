const express = require("express");
const router = express.Router();
const auth = require("../auth")

const userController = require("../controllers/user")


router.post('/checkEmail', (req, res)=>{
	userController.checkEmailExists(req.body).then(result => res.send(result))
})


router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
})


//Route for checking if the user's email is already exist in the database
/*router.post("/signup", (req, res)=>{
	userController.signUp(req.body).then(result => res.send(result))
})*/

//Routes for User Registration
/*router.post("/signup", (req, res) => {
	userController.signUpUser(req.body).then(result => res.send(result));
})*/

//Routes for authenticating a user

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
})


router.get("/details", auth.verify, (req, res) => {

       	const userData = auth.decode(req.headers.authorization)	
	//get all task function
	//call the task controller
	userController.getProfile({userId: userData.id}).then(result => res.send(result));
})





//

router.post("/orders", auth.verify, (req, res) => {
	let data = {
		/*user = req.body.userId,*/
		userId: req.body.userId,
		productId: req.body.productId,
		price: req.body.price,
		quantity: req.body.quantity,
		totalAmount: req.body.price * req.body.price
	}
	userController.order(data).then(resultFromController => res.send(resultFromController));
})


router.get("/userDetails", auth.verify, (req, res) => {

       	const userData = auth.decode(req.headers.authorization)	
	//get all task function
	//call the task controller
	userController.getAllUsers({userId: userData.id}).then(result => res.send(result));
})


module.exports = router;


//
/*router.get("/orderDetails", auth.verify, (req, res) => {

       	const orderData = auth.decode(req.headers.authorization)	
	//get all task function
	//call the task controller
	userController.getAllUsers({ordersId: orderData._ordersId}).then(result => res.send(result));
})

*/




//set user as Admin
///:userId/setAsAdmin

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

const userData = auth.decode(req.headers.authorization)

	userController.setAsAdmin(req.params, userData).then(result => res.send(result));

})

//Starts editing here

router.get('/cart', userController.getCart);

router.post('/cart', userController.postCart);

router.post('/cart-delete-item', userController.postCartDeleteProduct);

router.post('/create-order', userController.postOrder);


//userCart

router.post("/userCart", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}

	userController.userCart(data).then(resultFromController => res.send(resultFromController));
})

//userOrder

router.post("/userCheckout", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}

	userController.userOrder(data).then(resultFromController => res.send(resultFromController));
})





















module.exports = router;

