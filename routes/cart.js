const express = require("express");
const {
  addItemToCart,
  addToCart,
  getCartItems,
  removeCartItems,
} = require("../controller/cart");
const router = express.Router();

router.post(
  "/user/cart/addtocart",
  addItemToCart
);
//router.post('/user/cart/addToCartByLogin', requireSignin, userMiddleware, addToCart);
router.post("/user/getCartItems", getCartItems);
//new update
router.post(
  "/user/cart/removeItem",
  removeCartItems
);

module.exports = router;